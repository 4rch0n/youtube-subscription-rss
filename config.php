<?php
/*/
	Copyright (C) 2014 4rch0n <konrad@4rnet.com>, Manuel Alejandro <>
	https://bitbucket.org/4rch0n/youtube-subscription-rss
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom
	the Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
	WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
	OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
/*/

	/*/
		google php api you can get here from src dir:
		https://github.com/google/google-api-php-client
	/*/
	
	set_include_path(get_include_path() . PATH_SEPARATOR . 'path/to/google/php/api');// CHANGE THIS!
	
	require_once 'Google/Client.php';
	use google\appengine\api\cloud_storage\CloudStorageTools;

	if(isset($_SERVER['APPLICATION_ID']))
	{
		$pathToTokenFile="gs://" . CloudStorageTools::getDefaultGoogleStorageBucketName() ."/";
	}
	else
	{
		/*/
			pathToTokenFile should be path to not public folder
		/*/
		$pathToTokenFile="path/to/token/folder";// CHANGE THIS!
	}
	
	$tokenFileName="my.token";// CHANGE THIS!

	/*/
		You can get all credentials here:
		https://console.developers.google.com/project
		create project-> then open created project->apis & auth -> credentials -> 
		-> create new client id -> Web application
		(Remember! You need to set oauth redirect to http://your.web.page/some/path/login.php during key registration)
	/*/
	
	$client_id = '';// CHANGE THIS!
	$client_secret = '';// CHANGE THIS!
	$applicationName = "";// CHANGE THIS!
	
	 /*/
		Developer Key you can obtain from https://console.developers.google.com/project
		open created project->apis & auth -> credentials -> create new key -> Server Key (allowed IP can be empty == all IPs are allowed)
	 /*/
	$developerKey = "";// CHANGE THIS!
	

	//MAGIC! DON'T TOUCH!***************************************//
	$redirect_uri =  "http://" . $_SERVER['HTTP_HOST'] . str_replace("\\", "/", $_SERVER["SCRIPT_NAME"]);
	$client = new Google_Client();
	$client->setAccessType('offline');
	$client->setClientId($client_id);
	$client->setApprovalPrompt("force");
	$client->setClientSecret($client_secret);
	$client->setRedirectUri($redirect_uri);
	$client->addScope("https://www.googleapis.com/auth/youtube");
	//**********************************************************//
?>