<?php
/*/
	Copyright (C) 2014 4rch0n <konrad@4rnet.com>, Manuel Alejandro <>
	https://bitbucket.org/4rch0n/youtube-subscription-rss
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom
	the Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
	WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
	OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
/*/

	include 'config.php';
	
	if(!isset($_GET['channel'])) die('Give me channel/user ID - from https://www.youtube.com/account_advanced');

	$channel = $_GET['channel'];

	$url = "http://gdata.youtube.com/feeds/base/users/".$channel."/newsubscriptionvideos";

	$token = file_get_contents($pathToTokenFile.$tokenFileName);
	$client->setAccessToken($token);

	$token = $client->getAccessToken();
	$token = json_decode($token);
	
	if($client->getAuth()->isAccessTokenExpired())
	{
		$client->refreshToken($token->refresh_token);
		$token = $client->getAccessToken();
		file_put_contents($pathToTokenFile.$tokenFileName,$token);
		$token = json_decode($token);
	}

	$access_token = $token->access_token;

	$curlHeadder = array(
							"Authorization: Bearer ".$access_token,
							"X-GData-Key: key=".$developerKey
						);
	$timeout = 0; 

	if (function_exists('curl_init'))
	{
    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, $applicationName);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $curlHeadder);
		$curlResponse = curl_exec ($ch); 
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$response['header'] = substr($curlResponse, 0, $header_size);
		$response['body'] = substr($curlResponse, $header_size);
    }
	else
	{
		//urlfetch for GAE.
    	$context = array('http' =>
					    array(
					        'method'  => 'get',
					        'header'  => "User-Agent: " . $applicationName . "\r\n" .
			    						 "Authorization: Bearer " . $access_token . "\r\n" .
										 "X-GData-Key: key=".$developerKey
					    )
		);
		
    	$context = stream_context_create($context);
		$result = file_get_contents($url, false, $context);

		$response['body'] = $result;
    }

	header('Content-type: application/rss+xml; charset=utf-8');
	print_r($response['body']);
	
?>
