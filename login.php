<?php
/*/
	Copyright (C) 2014 4rch0n <konrad@4rnet.com>, Manuel Alejandro <>
	https://bitbucket.org/4rch0n/youtube-subscription-rss

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom
	the Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
	WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
	OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
	OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
/*/

	session_start();
	include 'config.php';

	if (isset($_GET['code']))
	{
		$client->authenticate($_GET['code']);
		$_SESSION['access_token'] = $client->getAccessToken();
		header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
	}
	
	if (isset($_SESSION['access_token']) && $_SESSION['access_token'])
	{
		$client->setAccessToken($_SESSION['access_token']);
	}
	else
	{
		$authUrl = $client->createAuthUrl();
	}

	if (isset($_SESSION['access_token']))
	{
		print_r( $_SESSION['access_token'] );
		file_put_contents($pathToTokenFile . $tokenFileName, $_SESSION['access_token']);
		echo '<p><a class="login" href="'. $_SERVER['PHP_SELF'].'?logout">Log out!</a></p>';
	}
	else
	{
		echo '<a class="login" href="'.$authUrl.'">Connect Me!</a>';
		
	}

	if (isset($_REQUEST['logout']))
	{
		unset($_SESSION['access_token']);
	}
?>